from conan import ConanFile

class MylibConan(ConanFile):
    name = "mylib"
    version = "0.2"

    # Optional metadata
    url = "<Package recipe repository url here, for issues about the package>"
    license = "<Put the package license here>"
    description = "<Description of Mylib here>"

    exports_sources = "mylib/*" # "CMakeLists.txt", "src/*", "inc/*"


    def package(self):
        self.copy("*")

